import Backbone from 'backbone';

export default Backbone.Model.extend({
    defaults: {
        'id': null,
        'title': '',
        'done': false
    },

    toggle: function() {
        this.set('done', !this.get('done'));
    }
});
