import Backbone from 'backbone';
import Item from './Item';

export default Backbone.Collection.extend({
    model: Item,
    url: '/items',

    parse: function(data) {
        return data.items;
    }
});
