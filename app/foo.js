import fooHtml from './templates/foo.html!text';

class Foo {
    constructor($el) {
        this.$el = $el;
    }

    render() {
        var text = fooHtml;
        this.$el.html(text);
    }
}

export default Foo;
