import $ from 'jquery';
import Backbone from 'backbone';
import Router from './routers/Router';
import Item from './models/Item';
import ItemList from './models/ItemList';

var itemList = new ItemList();
itemList.fetch();

var router = new Router({ itemList: itemList });
Backbone.history.start({ pushState: true });

$(document).on('click', 'a.linknav', function(event) {
    event.preventDefault();
    router.navigate($(this).attr('href'), { trigger: true });
});
