import $ from 'jquery';
import Backbone from 'backbone';
import AppView from '../views/AppView';
import Foo from '../foo';

export default Backbone.Router.extend({
    initialize: function(options) {
        this.options = options;
        this.itemList = options.itemList;
    },

    routes: {
        '': 'index',
        'foo': 'foo'
    },

    index: function() {
        var appView = new AppView({ collection: this.itemList });
        appView.render();
    },

    foo: function() {
        var foo = new Foo($('#app'));
        foo.render();
    }
});
