import _ from 'underscore';
import Backbone from 'backbone';
import itemHtml from '../templates/item.html!text';

export default Backbone.View.extend({
    initialize: function(options) {
        this.options = options;
        this.listenTo(this.model, 'change', this.render);
    },

    tagName: 'li',

    template: _.template(itemHtml),

    events: {
        'click .remove': 'onRemove',
        'click .toggle': 'onToggle'
    },

    onRemove: function() {
        this.model.destroy({ wait: true });
    },

    onToggle: function() {
        this.model.toggle();
        this.model.save({ wait: true });
    },

    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});
