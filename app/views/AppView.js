import _ from 'underscore';
import Backbone from 'backbone';
import Item from '../models/Item';
import ItemView from './ItemView';
import appHtml from '../templates/app.html!text';

export default Backbone.View.extend({
    initialize: function(options) {
        this.options = options;
        this.listenTo(this.collection, 'add remove', _.debounce(this.render, 0));

        this.$el.html(appHtml);
        this.$list = this.$('#item-list');
    },

    el: '#app',

    events: {
        'click #remove-checked-button': 'onRemoveChecked',
        'submit #new-item-form': 'onAddItem'
    },

    onRemoveChecked: function() {
        var checkedItems = this.collection.filter(function(item) {
            return item.get('done') == true;
        });

        checkedItems.forEach(function(item) {
            item.destroy({ wait: true });
        });
    },

    onAddItem: function(event) {
        event.preventDefault();

        if (!this.$('#new-item-title').val().length > 0) {
            return;
        }

        var item = new Item({ title: this.$('#new-item-title').val() });
        item.escape('title');

        this.$('#new-item-title').val('');

        this.collection.create(item, { wait: true });
    },

    render: function() {
        var list = this.$list;

        list.html('');
        this.collection.forEach(function(item) {
            var itemView = new ItemView({ model: item });
            list.append(itemView.render().el);
        });
    }
});
