### Make Commands
- `make init` - Run `npm install` and `jspm install`
- `make bundle` - Bundle the application into single `bundle.js` file (optional)
- `make clean-bundle` - Remove `bundle.js*`
- `make clean-db` - Remove `app.db`
- `make clean` - `clean-bundle` and `clean-db` combined
- `make runserver` - Run `node server.js`
