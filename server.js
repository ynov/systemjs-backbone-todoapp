'use strict';

var _ = require('underscore');
var fs = require('fs');
var Promise = Promise || require('promise');
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('app.db');

class Item {
    constructor(options) {
        this.id = options.id || null;
        this.title = options.title || '';
        this.done = options.done || false;
    }
}

class ItemService {
    createTable(callback) {
        return new Promise(function(fulfill, reject) {
            db.serialize(function() {
                db.run("CREATE TABLE item (id INTEGER PRIMARY KEY, title VARCHAR(255), done BOOLEAN)", function(err) {
                    if (!err) {
                        fulfill();
                    } else {
                        reject(err);
                    }
                });
            });
        });
    }

    all() {
        return new Promise(function(fulfill, reject) {
            db.serialize(function() {
                db.all("SELECT * FROM item", function(err, rows) {
                    if (err) {
                        return reject(err);
                    }

                    var items = [];
                    rows.forEach(function(row) {
                        var item = new Item({ id: row.id,
                                              title: row.title,
                                              done: row.done == 1 ? true : false });
                        items.push(item);
                    });
                    fulfill(items);
                });
            });
        });
    }

    add(item, callback) {
        item.title = item.title.replace(/>/g,'&gt;')
                               .replace(/</g,'&lt;');

        return new Promise(function(fulfill, reject) {
            db.serialize(function() {
                var stmt = db.prepare("INSERT INTO item (title, done) VALUES (?, ?)");
                stmt.run(item.title, item.done, function(err) {
                    if (err) {
                        reject(err);
                    }
                });
                stmt.finalize();

                db.get("SELECT last_insert_rowid() as id", function(err, row) {
                    if (err) {
                        return reject(err);
                    }
                    item.id = parseInt(row.id);
                    if (fulfill) {
                        fulfill(item);
                    }
                });
            });
        });
    }

    get(id, callback) {
        return new Promise(function(fulfill, reject) {
            db.serialize(function() {
                var stmt = db.prepare("SELECT * FROM item WHERE id = ? LIMIT 1");
                stmt.get(id, function(err, row) {
                    if (err) {
                        return reject(err);
                    } else if (!row) {
                        return reject('Item not found');
                    }
                    var item = new Item({ id: row.id,
                                          title: row.title,
                                          done: row.done == 1 ? true : false });
                    fulfill(item);
                });
                stmt.finalize();
            });
        });
    }

    update(item, callback) {
        return new Promise(function(fulfill, reject) {
            db.serialize(function() {
                var stmt = db.prepare("UPDATE item SET done = ? WHERE id = ?");
                stmt.run(item.done, item.id, function(err) {
                    if (err) {
                        return reject(err);
                    }
                    fulfill(item);
                });
                stmt.finalize();
            });
        });
    }

    remove(item, callback) {
        return new Promise(function(fulfill, reject) {
            db.serialize(function() {
                var stmt = db.prepare("DELETE FROM item WHERE id = ?");
                stmt.run(item.id, function(err) {
                    if (err) {
                        return reject(err);
                    }
                    fulfill(item);
                });
                stmt.finalize();
            });
        });
    }
}

var itemService = new ItemService();

itemService.createTable().then(function() {
    console.log("Table created!");
    itemService.add(new Item({ title: 'Say hello', done: true }));
    itemService.add(new Item({ title: 'Bundle application' }));
    itemService.add(new Item({ title: 'Install dependencies' }));
    console.log("Items seeded!");
});

app.use(express.static(__dirname));
app.use(bodyParser.json());

var _err = function(res, status) {
    status = status || 500;
    return function(err) {
        res.status(status).json({ error: err });
    }
}

// CRUD
app.get('/items', function(req, res) {
    itemService.all()
    .then(function(items) {
        res.json({ items: items });
    }, _err(res));
});

app.post('/items', function(req, res) {
    itemService.add(new Item(req.body))
    .then(function(item) {
        res.json(item);
    }, _err(res));
});

app.get('/items/:id', function(req, res) {
    itemService.get(req.params.id)
    .then(function(item) {
        res.json(item);
    }, _err(res, 404));
});

app.put('/items/:id', function(req, res) {
    itemService.get(req.params.id)
    .then(function(item) {
        item.done = req.body.done;
        return itemService.update(item);
    }, _err(res))
    .then(function(item) {
        res.json(item);
    }, _err(res));
});

app.delete('/items/:id', function(req, res) {
    itemService.get(req.params.id)
    .then(function(item) {
        return itemService.remove(item)
    }, _err(res))
    .then(function(item) {
        res.json(item);
    }, _err(res));
});

// URL Rewrite
app.get('*', function(req, res) {
    res.sendfile('./index.html');
});

console.log('http://localhost:3000');
app.listen(3000);
