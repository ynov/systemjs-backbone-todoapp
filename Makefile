npm=npm
node=node
rm=rm
jspm=./node_modules/.bin/jspm

bundle:
	$(jspm) bundle --minify app/main bundle.js

init:
	$(npm) install
	$(jspm) install
	echo "" > bundle.js

clean-db:
	$(rm) -f app.db

clean-bundle:
	$(rm) -f bundle.js
	$(rm) -f bundle.js.map
	echo "" > bundle.js

clean: clean-db clean-bundle

runserver:
	$(node) server.js

.PHONY: build init runserver clean clean-db clean-bundle
